package com.whty.zdxt.multimedia.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FFStream {

    // 视频/音频 编码名称
    @JsonProperty("codec_name")
    private String codecName;

    // 视频 video / 音频 audio
    @JsonProperty("codec_type")
    private String codecType;

    // 视频宽度
    private Integer width;

    // 视频高度
    private Integer height;

    // 视频帧率
    @JsonProperty("r_frame_rate")
    private String rFrameRate;

    // 每秒帧数
    private String frameRate;

    // 视频/音频 时长
    private String duration;

    // 视频/音频 比特率
    @JsonProperty("bit_rate")
    private Integer bitRate;

    // 总帧数
    @JsonProperty("nb_frames")
    private Integer nbFrames;

    // 音频采样率
    @JsonProperty("sample_rate")
    private Integer sampleRate;

    // 声道数 1：单声道 2：双声道
    private Integer channels;

    public String getCodecName() {
        return codecName;
    }

    public void setCodecName(String codecName) {
        this.codecName = codecName;
    }

    public String getCodecType() {
        return codecType;
    }

    public void setCodecType(String codecType) {
        this.codecType = codecType;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getrFrameRate() {
        return rFrameRate;
    }

    public void setrFrameRate(String rFrameRate) {
        this.rFrameRate = rFrameRate;
    }

    public String getFrameRate() {
        return frameRate;
    }

    public void setFrameRate(String frameRate) {
        this.frameRate = frameRate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getBitRate() {
        return bitRate;
    }

    public void setBitRate(Integer bitRate) {
        this.bitRate = bitRate;
    }

    public Integer getNbFrames() {
        return nbFrames;
    }

    public void setNbFrames(Integer nbFrames) {
        this.nbFrames = nbFrames;
    }

    public Integer getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(Integer sampleRate) {
        this.sampleRate = sampleRate;
    }

    public Integer getChannels() {
        return channels;
    }

    public void setChannels(Integer channels) {
        this.channels = channels;
    }

    @Override
    public String toString() {
        return "FFStream [" +
                "codecName=" + codecName +
                ", codecType=" + codecType +
                ", width=" + width +
                ", height=" + height +
                ", rFrameRate=" + rFrameRate +
                ", frameRate=" + frameRate +
                ", duration=" + duration +
                ", bitRate=" + bitRate +
                ", nbFrames=" + nbFrames +
                ", sampleRate=" + sampleRate +
                ", channels=" + channels +
                ']';
    }
}