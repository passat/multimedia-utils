package com.whty.zdxt.multimedia.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FFFormat {

    private String filename;

    @JsonProperty("nb_streams")
    private Integer nbStreams;

    private String duration;

    private String size;

    @JsonProperty("bit_rate")
    private String bitRate;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getNbStreams() {
        return nbStreams;
    }

    public void setNbStreams(Integer nbStreams) {
        this.nbStreams = nbStreams;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBitRate() {
        return bitRate;
    }

    public void setBitRate(String bitRate) {
        this.bitRate = bitRate;
    }

    @Override
    public String toString() {
        return "FFFormat [" +
                "filename=" + filename +
                ", nbStreams=" + nbStreams +
                ", duration=" + duration +
                ", size=" + size +
                ", bitRate=" + bitRate +
                ']';
    }
}