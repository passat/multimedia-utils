package com.whty.zdxt.multimedia.pojo;

import java.util.List;

public class FFmpegInfo {
    private List<FFStream> streams;
    private FFFormat format;
    private AudioInfo audioInfo;
    private VideoInfo videoInfo;

    public List<FFStream> getStreams() {
        return streams;
    }

    public void setStreams(List<FFStream> streams) {
        this.streams = streams;
    }

    public FFFormat getFormat() {
        return format;
    }

    public void setFormat(FFFormat format) {
        this.format = format;
    }

    public AudioInfo getAudioInfo() {
        return audioInfo;
    }

    public void setAudioInfo(AudioInfo audioInfo) {
        this.audioInfo = audioInfo;
    }

    public VideoInfo getVideoInfo() {
        return videoInfo;
    }

    public void setVideoInfo(VideoInfo videoInfo) {
        this.videoInfo = videoInfo;
    }

    @Override
    public String toString() {
        return "FFmpegInfo [" +
                "streams=" + streams +
                ", format=" + format +
                ", audioInfo=" + audioInfo +
                ", videoInfo=" + videoInfo +
                ']';
    }
}
