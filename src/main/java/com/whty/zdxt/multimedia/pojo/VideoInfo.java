package com.whty.zdxt.multimedia.pojo;

public class VideoInfo {
    // 视频 编码名称
    private String codecName;

    // 视频 video
    private String codecType;

    // 视频 宽度
    private Integer width;

    // 视频 高度
    private Integer height;

    // 视频 帧率
    private Integer frameRate;

    // 视频 时长
    private String duration;

    // 视频 比特率
    private Integer bitRate;

    // 视频 总帧数
    private Integer nbFrames;

    public String getCodecName() {
        return codecName;
    }

    public void setCodecName(String codecName) {
        this.codecName = codecName;
    }

    public String getCodecType() {
        return codecType;
    }

    public void setCodecType(String codecType) {
        this.codecType = codecType;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getFrameRate() {
        return frameRate;
    }

    public void setFrameRate(Integer frameRate) {
        this.frameRate = frameRate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getBitRate() {
        return bitRate;
    }

    public void setBitRate(Integer bitRate) {
        this.bitRate = bitRate;
    }

    public Integer getNbFrames() {
        return nbFrames;
    }

    public void setNbFrames(Integer nbFrames) {
        this.nbFrames = nbFrames;
    }

    @Override
    public String toString() {
        return "VideoInfo [" +
                "codecName=" + codecName +
                ", codecType=" + codecType +
                ", width=" + width +
                ", height=" + height +
                ", frameRate=" + frameRate +
                ", duration=" + duration +
                ", bitRate=" + bitRate +
                ", nbFrames=" + nbFrames +
                ']';
    }
}