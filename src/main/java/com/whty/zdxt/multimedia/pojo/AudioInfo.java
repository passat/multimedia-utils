package com.whty.zdxt.multimedia.pojo;

public class AudioInfo {
    // 音频 编码名称
    private String codecName;

    // 音频 audio
    private String codecType;

    // 音频 时长
    private String duration;

    // 音频 比特率
    private Integer bitRate;

    // 音频 采样率
    private Integer sampleRate;

    // 音频 声道数 1：单声道 2：双声道
    private Integer channels;

    public String getCodecName() {
        return codecName;
    }

    public void setCodecName(String codecName) {
        this.codecName = codecName;
    }

    public String getCodecType() {
        return codecType;
    }

    public void setCodecType(String codecType) {
        this.codecType = codecType;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getBitRate() {
        return bitRate;
    }

    public void setBitRate(Integer bitRate) {
        this.bitRate = bitRate;
    }

    public Integer getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(Integer sampleRate) {
        this.sampleRate = sampleRate;
    }

    public Integer getChannels() {
        return channels;
    }

    public void setChannels(Integer channels) {
        this.channels = channels;
    }

    @Override
    public String toString() {
        return "AudioInfo [" +
                "codecName=" + codecName +
                ", codecType=" + codecType +
                ", duration=" + duration +
                ", bitRate=" + bitRate +
                ", sampleRate=" + sampleRate +
                ", channels=" + channels +
                ']';
    }
}