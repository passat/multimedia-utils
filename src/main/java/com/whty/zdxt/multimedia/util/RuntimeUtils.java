package com.whty.zdxt.multimedia.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author GuoNanLin
 * @date 2020-10-30
 */
class RuntimeUtils {
    private static final Log log = LogFactory.getLog(RuntimeUtils.class);

    /**
     * 执行cmd命令,打印成功失败信息
     */
    public static void execSF(String command) {
        log.debug("执行命令：" + command);
        Process exec = null;
        try {
            exec = Runtime.getRuntime().exec(command);
            exec.waitFor();
            RuntimeUtils.successful(exec);
            String failure = RuntimeUtils.failure(exec);
            int i = exec.exitValue();
            if (i != 0) {
                throw new RuntimeException(failure);
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("执行命令失败", e);
        } finally {
            if (exec != null) {
                exec.destroy();
            }
        }
    }

    /**
     * 执行cmd命令,打印失败信息
     */
    public static void execFailure(String command) {
        log.debug("执行命令：" + command);
        Process exec = null;
        try {
            exec = Runtime.getRuntime().exec(command);
            exec.waitFor();
            String failure = RuntimeUtils.failure(exec);
            int i = exec.exitValue();
            if (i != 0) {
                throw new RuntimeException(failure);
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("执行命令失败", e);
        } finally {
            if (exec != null) {
                exec.destroy();
            }
        }
    }

    /**
     * 执行cmd命令,打印失败信息
     */
    public static String execSuccessful(String command) {
        log.debug("执行命令：" + command);
        Process exec = null;
        String response = null;
        try {
            exec = Runtime.getRuntime().exec(command);
            response = RuntimeUtils.successful(exec);
        } catch (IOException e) {
            throw new RuntimeException("执行命令失败", e);
        } finally {
            if (exec != null) {
                exec.destroy();
            }
        }
        return response;
    }

    /**
     * 获取执行成功结果
     */
    private static String successful(Process exec) throws IOException {
        InputStream is = exec.getInputStream();
        return response(is);
    }

    /**
     * 获取执行失败结果
     */
    private static String failure(Process exec) throws IOException {
        InputStream is = exec.getErrorStream();
        return response(is);
    }

    /**
     * 打印执行结果
     *
     * @param is 结果输入流
     */
    private static String response(InputStream is) throws IOException {
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
        String r = "";
        while ((line = br.readLine()) != null) {
//            System.out.println(line);
            r = r + line;
        }
        return r;
    }
}
