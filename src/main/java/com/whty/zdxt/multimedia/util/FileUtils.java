package com.whty.zdxt.multimedia.util;


import java.util.UUID;

/**
 * @author GuoNanLin
 * @date 2020-10-30
 */
class FileUtils {

    /**
     * 获取文件后缀名
     *
     * @param fileName
     */
    public static String getSuffix(String fileName) {
        if (fileName == null) {
            throw new RuntimeException("获取文件拓展名失败");
        }
        int index = fileName.lastIndexOf(".");
        if (-1 == index) {
            throw new RuntimeException("获取文件拓展名失败");
        }
        return fileName.substring(index);
    }

    /**
     * 根据  suffix 新建文件名
     *
     * @param suffix 后缀名
     */
    public static String createFileName(String suffix) {
        return UUID.randomUUID().toString() + suffix;
    }

    /**
     * 根据文件名获取文件路径
     *
     * @param tempDirectory 临时文件路径
     * @param fileName      文件名
     */
    public static String getFilePath(String tempDirectory, String fileName) {
        return tempDirectory + "/" + fileName;
    }

    public static boolean checkFileName(String fileName){
        if(fileName == null){
            return false;
        }

        if(fileName.contains("/")||fileName.contains("\\")){
            return false;
        }

        if(!fileName.contains(".")){
            return false;
        }
        return true;
    }

}
