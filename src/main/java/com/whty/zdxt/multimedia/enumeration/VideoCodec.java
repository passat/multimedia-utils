package com.whty.zdxt.multimedia.enumeration;

public enum VideoCodec {
    LIBX264("libx264"),
    LIBX265("libx265");

    private String code;

    VideoCodec(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
