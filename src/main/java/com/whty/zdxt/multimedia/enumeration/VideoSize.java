package com.whty.zdxt.multimedia.enumeration;

public enum VideoSize {
    HD480("hd480", 852, 480),
    HD720("hd720", 1280, 720),
    HD1080("hd1080", 1920, 1080),
    XY800("800x800", 800, 800);

    private String code;
    private Integer x;
    private Integer y;

    VideoSize(String code, Integer x, Integer y) {
        this.code = code;
        this.x = x;
        this.y = y;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
