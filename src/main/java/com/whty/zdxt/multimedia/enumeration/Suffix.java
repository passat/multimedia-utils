package com.whty.zdxt.multimedia.enumeration;

public enum Suffix {
    JPG(".jpg"),
    PNG(".png"),
    WEBP(".webp"),
    WEBP_LOSSLESS(".webp"),
    MP4(".mp4");

    private String code;

    Suffix(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
