package com.whty.zdxt.multimedia.attribute;


public class CompressionAttributes {
    /**
     * 音频压缩参数
     */
    private AudioAttributes audioAttributes;

    /**
     * 视频压缩参数
     */
    private VideoAttributes videoAttributes;

    /**
     * 视频压缩完成后的回调地址
     */
    private String progressUrl;

    public CompressionAttributes() {
    }

    public CompressionAttributes(VideoAttributes videoAttributes, String progressUrl) {
        this.videoAttributes = videoAttributes;
        this.progressUrl = progressUrl;
    }

    public CompressionAttributes(AudioAttributes audioAttributes, String progressUrl) {
        this.audioAttributes = audioAttributes;
        this.progressUrl = progressUrl;
    }

    public CompressionAttributes(AudioAttributes audioAttributes, VideoAttributes videoAttributes, String progressUrl) {
        this.audioAttributes = audioAttributes;
        this.videoAttributes = videoAttributes;
        this.progressUrl = progressUrl;
    }

    public AudioAttributes getAudioAttributes() {
        return audioAttributes;
    }

    public void setAudioAttributes(AudioAttributes audioAttributes) {
        this.audioAttributes = audioAttributes;
    }

    public VideoAttributes getVideoAttributes() {
        return videoAttributes;
    }

    public void setVideoAttributes(VideoAttributes videoAttributes) {
        this.videoAttributes = videoAttributes;
    }

    public String getProgressUrl() {
        return progressUrl;
    }

    public void setProgressUrl(String progressUrl) {
        this.progressUrl = progressUrl;
    }

    @Override
    public String toString() {
        return "CompressionAttributes [" +
                "audioAttributes=" + audioAttributes +
                ", videoAttributes=" + videoAttributes +
                ", progressUrl=" + progressUrl +
                ']';
    }
}
