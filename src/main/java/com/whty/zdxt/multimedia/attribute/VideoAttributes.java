package com.whty.zdxt.multimedia.attribute;

import com.whty.zdxt.multimedia.enumeration.VideoCodec;
import com.whty.zdxt.multimedia.enumeration.VideoSize;

public class VideoAttributes {

    /**
     * 视频编码
     */
    private VideoCodec codec;

    /**
     * 压缩后最大帧率 例：20
     * 未设置则保持同原视频一致
     */
    private Integer maxFps;

    /**
     * 压缩后最大比特率 例：128000
     */
    private Integer maxBitRate;

    /**
     * 输出视频分辨率
     * 未设置则保持同原视频一致
     */
    private VideoSize videoSize;

    /**
     * 压缩视频最大时长
     * 未设置则保持同原视频一致
     */
    private Integer maxDuration;


    public VideoCodec getCodec() {
        return codec;
    }

    public void setCodec(VideoCodec codec) {
        this.codec = codec;
    }

    public Integer getMaxFps() {
        return maxFps;
    }

    public void setMaxFps(Integer maxFps) {
        this.maxFps = maxFps;
    }

    public Integer getMaxBitRate() {
        return maxBitRate;
    }

    public void setMaxBitRate(Integer maxBitRate) {
        this.maxBitRate = maxBitRate;
    }

    public VideoSize getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(VideoSize videoSize) {
        this.videoSize = videoSize;
    }

    public Integer getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(Integer maxDuration) {
        this.maxDuration = maxDuration;
    }

    @Override
    public String toString() {
        return "VideoAttributes [" +
                "codec=" + codec +
                ", maxFps=" + maxFps +
                ", maxBitRate=" + maxBitRate +
                ", videoSize=" + videoSize +
                ", maxDuration=" + maxDuration +
                ']';
    }
}
