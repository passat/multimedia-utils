package com.whty.zdxt.multimedia.attribute;

public class CropAttributes {
    /**
     * 裁切图片的宽
     */
    private Integer width;

    /**
     * 裁切图片的高
     */
    private Integer height;

    /**
     * 距左边偏移量
     */
    private Integer leftOffset;

    /**
     * 距顶部偏移量
     */
    private Integer topOffset;

    public CropAttributes() {
    }

    public CropAttributes(Integer width, Integer height, Integer leftOffset, Integer topOffset) {
        this.width = width;
        this.height = height;
        this.leftOffset = leftOffset;
        this.topOffset = topOffset;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getLeftOffset() {
        return leftOffset;
    }

    public void setLeftOffset(Integer leftOffset) {
        this.leftOffset = leftOffset;
    }

    public Integer getTopOffset() {
        return topOffset;
    }

    public void setTopOffset(Integer topOffset) {
        this.topOffset = topOffset;
    }

    @Override
    public String toString() {
        return "CropAttributes [" +
                "width=" + width +
                ", height=" + height +
                ", leftOffset=" + leftOffset +
                ", topOffset=" + topOffset +
                ']';
    }
}
