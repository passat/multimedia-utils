package com.whty.zdxt.multimedia.attribute;

public class ResizeAttributes {

    /**
     * 输出图片的宽
     */
    private Integer width;

    /**
     * 输出图片的高
     */
    private Integer height;

    /**
     * 输出图片的质量 0-100
     * 未设置则为100
     */
    private Integer quality;
    
    public ResizeAttributes() {
    }

    public ResizeAttributes(Integer width, Integer height) {
        this.width = width;
        this.height = height;
    }

    public ResizeAttributes(Integer width, Integer height, Integer quality) {
        this.width = width;
        this.height = height;
        this.quality = quality;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    @Override
    public String toString() {
        return "ResizeAttributes [" +
                "width=" + width +
                ", height=" + height +
                ", quality=" + quality +
                ']';
    }
}
