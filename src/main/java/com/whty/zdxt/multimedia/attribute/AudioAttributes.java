package com.whty.zdxt.multimedia.attribute;

public class AudioAttributes {

    /**
     * 压缩后最大比特率 例：128000
     * 未设置则保持同原视频一致
     */
    private Integer maxBitRate;

    /**
     * 压缩后最大采样率 例：44100
     * 未设置则保持同原视频一致
     */
    private Integer maxSamplingRate;

    public AudioAttributes() {
    }

    public AudioAttributes(Integer maxBitRate, Integer maxSamplingRate) {
        this.maxBitRate = maxBitRate;
        this.maxSamplingRate = maxSamplingRate;
    }

    public Integer getMaxBitRate() {
        return maxBitRate;
    }

    public void setMaxBitRate(Integer maxBitRate) {
        this.maxBitRate = maxBitRate;
    }

    public Integer getMaxSamplingRate() {
        return maxSamplingRate;
    }

    public void setMaxSamplingRate(Integer maxSamplingRate) {
        this.maxSamplingRate = maxSamplingRate;
    }

    @Override
    public String toString() {
        return "AudioAttributes [" +
                "maxBitRate=" + maxBitRate +
                ", maxSamplingRate=" + maxSamplingRate +
                ']';
    }
}
